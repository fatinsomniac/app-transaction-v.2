<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/







Auth::routes();

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/home', 'HomeController@index')->name('home');
//============================== KARYAWAN ===========================================

Route::get('/karyawan/monitoring', 'KaryawanController@viewTransaction')->middleware('auth');
Route::get('/karyawan/create', 'KaryawanController@create')->middleware('auth');
Route::get('/karyawan/edit/{id}', 'KaryawanController@edit')->middleware('auth');
Route::get('/karyawan/search', 'KaryawanController@search')->name('karyawan.search')->middleware('auth');
Route::get('/karyawan/searchDate', 'KaryawanController@searchDate')->name('karyawan.searchDate')->middleware('auth');
Route::post('/transactions/store', 'KaryawanController@store')->middleware('auth');
Route::patch('/karyawan/update/{id}', 'KaryawanController@update')->middleware('auth');


//================================== ADMIN ========================================

Route::get('/admin/dashboard', 'AdminController@viewAdmin')->middleware('auth');
Route::get('/admin/create', 'AdminController@create')->middleware('auth');
Route::get('/admin/edit/{id}', 'AdminController@edit')->middleware('auth');
Route::get('/admin/search', 'AdminController@search')->name('admin.search')->middleware('auth');
Route::get('/admin/searchDate', 'AdminController@searchDate')->name('admin.searchDate')->middleware('auth');
Route::post('/admin/store', 'AdminController@store')->middleware('auth');
Route::patch('/admin/update/{id}', 'AdminController@update')->middleware('auth');
Route::delete('/admin/delete/{id}', 'AdminController@delete')->middleware('auth');
