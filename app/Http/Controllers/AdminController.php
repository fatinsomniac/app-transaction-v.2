<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use Auth;

class AdminController extends Controller
{
    public function __construc()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        return view('admin.dashboard');
    }

    public function create()
    {
        return view('admin.create');
    }

    public function store(Request $request)
    {
        // VALIDASI
        $this->validate($request, [
            'name_employees' => 'required',
            'price' => 'required|numeric',
            'item' => 'required',
            'file.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:3048',
            'keterangan' => 'required'
        ]);
        // END VALIDASI


        if ($request->hasfile('file')) {
            foreach ($request->file('file') as $file) {
                $name = $file->getClientOriginalName();
                $file->move(storage_path() . '/app/public/images/transactions', $name);
                $data[] = $name;
                $upload_model = new Transaction;
                $upload_model->name_employees = $request->name_employees;
                $upload_model->price = $request->price;
                $upload_model->item = $request->item;
                $upload_model->keterangan = $request->keterangan;
                $upload_model->status = "Casbon";
                $upload_model->file = json_encode($data);
            }
        }

        $transaction = auth()->user()->transactions()->save($upload_model);
        return back()->with('message', 'User has successfully Created');
    }

    public function viewAdmin()
    {
        $admin = Transaction::paginate(5);
        return view('admin/dashboard', compact('admin'));
    }

    public function edit($id)
    {
        $ubah = Transaction::find($id);
        return view('admin/edit', compact('ubah'));
    }

    public function update(Request $request, $id)
    {
        $upload_model = Transaction::find($id);
        if ($request->file('file')) {
            \Storage::delete($upload_model->file);
            if($request->hasfile('file'))
            {
                foreach($request->file('file') as $file)
                {
                    $name = $file->getClientOriginalName();                
                    $file->move(storage_path().'/app/public/images/transactions', $name);
                    $data []= $name;  
                    $upload_model->file = json_encode($data);         
                }
            }
        } else {
            $kirim = json_encode($upload_model->file);
            // dd($kirim);
        }
        $upload_model->name_employees = $request->name_employees;
        $upload_model->price = $request->price;
        $upload_model->status = $request->status;
        //Update data
        $upload_model->update();
        return redirect('/admin/dashboard')->with('message', 'User has successfully Update');
    }

    public function delete($id)
    {
        Transaction::destroy($id);
        return back();
    }

    public function search(Request $request)
    {
        $query = $request->get('query');

        $admin = Transaction::where("name_employees", "like", "%$query%")
            ->orwhere("status", "like", "%$query%")->latest()->paginate();



        return view('/admin/dashboard', compact('admin'));
    }
    public function searchDate(Request $request)
    {
        $query = $request->get('query');

        $admin = Transaction::where("created_at", "like", "%$query%")->latest()->paginate();

        return view('/admin/dashboard', compact('admin'));
    }
}
