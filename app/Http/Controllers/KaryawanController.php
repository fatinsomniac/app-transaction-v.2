<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Transaction;
use Auth;

class KaryawanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function create()
    {
        return view('karyawan.create');
    }

    public function store(Request $request)
    {
        // VALIDASI
        $this->validate($request,[
            'name_employees' => 'required',
            'price' => 'required|numeric',            
            'file.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:3048'
             ]);
        // END VALIDASI
            

        if($request->hasfile('file'))
        {
            foreach($request->file('file') as $file)
            {
                $name = $file->getClientOriginalName();                
                $file->move(storage_path().'/app/public/images/transactions', $name);
                $data []= $name;  
                $upload_model = new Transaction;
                $upload_model->name_employees = $request->name_employees;
                $upload_model->price = $request->price;
                $upload_model->status = "Casbon";
                $upload_model->file = json_encode($data);         
             }
        }
       
        $transaction = auth()->user()->transactions()->save($upload_model);
        return back()->with('message', 'User has successfully Created');
    }


    public function viewTransaction()
    {
        $read = Transaction::paginate(5);    
        return view('karyawan.monitoring', compact('read'));
        
    }

    public function edit($id)
    {
        $trans = Transaction::find($id);
        return view('karyawan.edit', compact('trans'));
    }    

    public function update(Request $request, $id)
    {
        $upload_model = Transaction::find($id);
        if ($request->file('file')) {
            \Storage::delete($upload_model->file);
            if($request->hasfile('file'))
            {
                foreach($request->file('file') as $file)
                {
                    $name = $file->getClientOriginalName();                
                    $file->move(storage_path().'/app/public/images/transactions', $name);
                    $data []= $name;  
                    $upload_model->file = json_encode($data);         
                }
            }
        } else {
            $kirim = json_encode($upload_model->file);
            // dd($kirim);
        }
        $upload_model->name_employees = $request->name_employees;
        $upload_model->price = $request->price;
        $upload_model->status = "Casbon";
        //Update data
        $upload_model->update();
        return redirect('/karyawan/monitoring')->with('message', 'User has successfully Update');
    }

    public function search(Request $request)
    {
        $query = $request->get('query');
        $from = date('Y-m-d');
        $to = date('now'); 

        $read = Transaction::where("name_employees", "like", "%$query%")
                        ->whereBetween('created_at', [$from." 00:00:00",$to." 23:59:59"])->first()->paginate(6);
       
        return view('/karyawan/monitoring', compact('read'));
    }
    
   

}
