@extends('layouts.template')

@section('style')
<link rel="stylesheet" href="{{asset('style/bootstrap.min.css')}}">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
@endsection

@section('content')
<div class="container mt-5" style="width: 33%;">
    {{-- METHOD ERROR FORMULIR --}}
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <strong>Sorry !</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif

    @if(session('success'))
    <div class="alert alert-success">
      {{ session('success') }}
    </div> 
    @endif
{{-- END METHOD ERROR --}} 
<form class="mb-4" action="/karyawan/update/{{$trans->id}}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('PATCH')
  <div class="card card-body border-primary mb-3">
    <h1 class="text-center mb-4">Edit Transaction</h1>
    <div class="form-group">
        <label for="">Nama Karyawan</label>
        <input value="{{$trans->name_employees}}" type="text" class="form-control" name="name_employees">
    </div>
    <div class="form-group">
        <label for="">Total Harga</label>
        <input value="{{$trans->price}}" type="number" class="form-control" name="price">
    </div>
    <div class="form-group">
      <label for="">Item Barang</label>
      <textarea type="text" class="form-control @error('item') is-invalid @enderror" name="item">
        {{ $trans->item}}
      </textarea>
      @error('item')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
      @enderror
   </div>   
    {{-- UPLOAD IMAGE --}}
    <div class="input-group control-group increment" >
        <input type="file" name="file[]" class="form-control">
        <div class="input-group-btn"> 
          <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
        </div>
      </div>
      <div class="clone hide">
        <div class="control-group input-group" style="margin-top:10px">
          <input value="{{$trans->file}}" type="file" name="file[]" class="form-control" id="file"  multiple='multiple' aria-describedby="fileHelp">
          <div class="input-group-btn"> 
            <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
          </div>
        </div>
      </div>
      {{-- END UPLOAD IMAGE --}} 
      <div class="form-group">
        <label for="">Keterangan</label>
        <textarea type="text" class="form-control @error('keterangan') is-invalid @enderror" name="keterangan">
          {{ $trans->keterangan}}
        </textarea>
        @error('keterangan')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror
      </div>         
    <button type="submit" id="btn-submit" class="btn btn-primary mt-3">Update</button>
  </div> 
</form>
{{-- JQUERY --}}
<script type="text/javascript">
    $(document).ready(function() {
      $(".btn-success").click(function(){ 
          var html = $(".clone").html();
          $(".increment").after(html);
      });
      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".control-group").remove();
      });
    });
</script>
{{-- END JQUERY --}}
    
@endsection