@extends('layouts.template')

@section('style')
<link rel="stylesheet" href="{{asset('style/bootstrap.min.css')}}"> 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> 
@endsection

@section('content')
<div class="container mt-5" style="width: 30%;">
  
  {{-- METHOD ERROR FORMULIR --}}
  @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Sorry !</strong> There were some problems with your input.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

        @if(session('success'))
        <div class="alert alert-success">
          {{ session('success') }}
        </div> 
        @endif
  {{-- END METHOD ERROR --}}

  <form class="mb-6" action="/transactions/store" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="card card-body border-primary mb-3">
      <h1 class="text-center mb-4 ">Create Transaction</h1>
      <div class="form-group ">
          <label for="">Nama Karyawan</label>
          <input type="text" class="form-control @error('name_employees') is-invalid @enderror" name="name_employees" >
          @error('name_employees')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
          @enderror
      </div>
      <div class="form-group">
          <label for="">Total Harga</label>
          <input type="number" class="form-control @error('price') is-invalid @enderror" name="price">
          @error('price')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
          @enderror
      </div>
      <div class="form-group">
        <label for="">Item Barang</label>
        <textarea type="text" class="form-control @error('item') is-invalid @enderror" name="item"></textarea>
        @error('item')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror
     </div>      

      {{-- UPLOAD IMAGE --}}
      <div class="input-group control-group increment" >
        <input type="file" name="file[]" class="form-control">
        <div class="input-group-btn"> 
          <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
        </div>
      </div>
      <div class="clone hide">
        <div class="control-group input-group" style="margin-top:10px">
          <input type="file" name="file[]" class="form-control">
          <div class="input-group-btn"> 
            <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
          </div>
        </div>
      </div>
      {{-- END UPLOAD IMAGE --}}
      <div class="form-group">
        <label for="">Keterangan</label>
        <textarea type="text" class="form-control @error('keterangan') is-invalid @enderror" name="keterangan"></textarea>
        @error('keterangan')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
        @enderror
      </div>
      <button type="submit" id="btn-submit" class="btn btn-primary mt-3">Submit</button>
    </div>
  </form>
  {{-- JQUERY --}}
  <script type="text/javascript">
    $(document).ready(function() {
      $(".btn-success").click(function(){ 
          var html = $(".clone").html();
          $(".increment").after(html);
      });
      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".control-group").remove();
      });
    });
</script>
{{-- END JQUERY --}}
</div>
<script src="{{asset('scripts/jquery-3.5.0.min.js')}}"></script>
<script src="{{asset('scripts/bootstrap.min.js')}}"></script>
    
@endsection