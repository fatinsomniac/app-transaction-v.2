@extends('layouts.admin')

@section('style')
<link rel="stylesheet" href="{{asset('style/bootstrap.min.css')}}"> 
@endsection

@section('content')
<div class="container mt-5" style="width: 33%;">
   
  <form class="mb-4" action="/admin/update/{{$ubah->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <div class="card card-body border-primary mb-3">
      <h1 class="text-center mb-4">Edit Transaction</h1>
      <div class="form-group">
          <label for="">Nama Karyawan</label>
          <input value="{{$ubah->name_employees}}" type="text" class="form-control" name="name_employees">
      </div>
      <div class="form-group">
          <label for="">Total Harga</label>          
          <input value="{{$ubah->price}}" type="number" class="form-control" name="price">
      </div>
      
      {{-- UPLOAD IMAGE --}}
    <div class="input-group control-group increment" >
      <input type="file" name="file" class="form-control">
      <div class="input-group-btn"> 
        <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
      </div>
    </div>
    <div class="clone hide">
      <div class="control-group input-group" style="margin-top:10px">
        <input value="{{$ubah->file}}" type="file" name="file" class="form-control" id="file"  multiple='multiple' 
        aria-describedby="fileHelp">
        <div class="input-group-btn"> 
          <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
        </div>
      </div>
    </div>
    
    {{-- END UPLOAD IMAGE --}}
        
      <div class="form-group">
          <label for="">Status</label>
          <select value="{{$ubah->status}}" type="text" class="form-control" name="status">
            <option >Sudah Bayar</option>
            <option >Casbon</option>            
          </select>          
      </div>      
      <button type="submit" id="btn-submit" class="btn btn-primary mt-3">Update</button>
    </div>
  </form>
  {{-- JQUERY --}}
<script type="text/javascript">
    $(document).ready(function() {
      $(".btn-success").click(function(){ 
          var html = $(".clone").html();
          $(".increment").after(html);
      });
      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".control-group").remove();
      });
    });
</script>
{{-- END JQUERY --}}
</div>
<script src="{{asset('scripts/jquery-3.5.0.min.js')}}"></script>
<script src="{{asset('scripts/bootstrap.min.js')}}"></script>
    
@endsection