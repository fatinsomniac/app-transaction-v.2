@extends('layouts.admin')

@section('style')
<link rel="stylesheet" href="{{asset('style/bootstrap.min.css')}}"> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>  
@endsection

@section('content')
<div class="container mt-4">
    <h1>Transaction List</h1>
    <form class="form-inline my-2 my-lg-0">      
      <div class="form-row">
          
          {{-- <div class="form-group col-md-4 mb-3">         
          <form class="form" action="{{route('karyawan.search')}}" method="GET">
            <div class="form-group mx-sm-3 mb-2">
              <label for="">From Date:</label>                          
              <input value="{{Request::get('from')}}" type="date" class="date form-control" id="from" name="from">
              <label for="">To Dtae:</label>                         
              <input value="{{Request::get('to')}}" type="date" class="date form-control" id="to" name="to"> 
              <button type="button" class="btn btn-success">Filter</button>
            </div>                           
         </form>
          </div>   --}}
      </div>
 </form>
    <div class="table-responsive"> 
    <table class="table table-striped table-bordered">
        <thead class="thead-dark ">
            <tr>
                <th>#</th>
                <th class="text-center" width="10%">Tgl Input</th>                    
                <th class="text-center">Nama Karyawan</th>                   
                <th class="text-center" width="15%">Total Pembayaran</th>
                <th class="text-center" width="5%">Item Barang</th>
                <th class="text-center" width="40%">Struk</th>
                <th class="text-center">Ketarangan</th>
                <th class="text-center">Status</th>                   
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($admin as $value)
              <tr>
              <td>{{$loop->iteration}}</td>
              <td>{{$value->created_at}}</td>
              <td>{{$value->name_employees}}</td>
              <td>@currency($value->price)</td>
              <td>{{$value->item}}</td>                  
              <td>
                @foreach(json_decode($value->file) as $picture)
                  <img src="{{ asset('/storage/images/transactions/'.$picture) }}" style="height:120px; width:200px"/>
                @endforeach 
              </td>
              <td>{{$value->keterangan}}</td>
              <td>{{$value->status}}</td>
                  <td>
                    <a href="/admin/edit/{{$value->id}}" class="btn btn-primary">Edit</a>
                    <form action="/admin/delete/{{$value->id}}" method="POST">
                      @csrf
                      @method('DELETE')
                         <button type="submit" class="btn btn-dark">Delete</button>
                      </form>                       
                  </td>
              </tr>
              @endforeach                        
        </tbody>
    </table>
    <div class="d-block col-12 mt-5 ">{{ $admin->links() }}</div>
  </div>
</div>  
    
@endsection