<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>App Transaction</title>
    @yield('style')
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css"
        rel="stylesheet">
    <link rel="stylesheet" href="{{asset('style/bootstrap.min.css')}}">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="#">CRUD</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item ">
                        <a class="nav-link" href="/karyawan/monitoring">Monitoring</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/karyawan/create">Create Transaction</a>
                    </li>

                    <li class="nav-item ">
                        <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                    </li>
                </ul>


            </div>
            <form class="form-inline" action="{{route('karyawan.search')}}" method="GET">
                <div class="form-group mx-sm-3 mb-2">
                    <input value="{{Request::get('query')}}" type="text" class="form-control mr-sm-2"
                        placeholder="Search by name .." name="query">
                </div>
            </form>
            <label class="text-white">OR</label>
            <form class="form-inline" action="{{route('karyawan.searchDate')}}" method="GET">
                <div class="form-group mx-sm-3 mb-2">
                    <input value="{{Request::get('query')}}" type="date" class="form-control mr-sm-2" name="query">
                    <button type="button" value="submit" class="btn btn-outline-warning">filter</button>
                </div>
            </form>


        </div>




    </nav>


    @include('message')


    @yield('content')
    </div>

</body>

</html>
